//
//  SwipeGestureViewController.swift
//  gesture-gr1-jg
//
//  Created by Jose Gonzalez on 17/1/18.
//  Copyright © 2018 Jose Gonzalez. All rights reserved.
//

import UIKit

class SwipeGestureViewController: UIViewController {

    
    
    @IBOutlet weak var customView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func swipeUpAction(_ sender: Any) {
        
        let action = sender as! UISwipeGestureRecognizer
        
        
        customView.backgroundColor = .black
    }
    
    @IBAction func swipeDownAction(_ sender: Any) {
             customView.backgroundColor = .blue
    }
    
    @IBAction func swipeLeftAction(_ sender: Any) {
             customView.backgroundColor = .red
    }
    
    @IBAction func swipeRightAction(_ sender: Any) {
        
        customView.backgroundColor = .green
    }
    
    
}
