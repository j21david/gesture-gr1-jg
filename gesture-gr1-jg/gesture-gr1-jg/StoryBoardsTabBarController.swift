//
//  StoryBoardsTabBarController.swift
//  gesture-gr1-jg
//
//  Created by Jose Gonzalez on 23/1/18.
//  Copyright © 2018 Jose Gonzalez. All rights reserved.
//

import UIKit

class StoryBoardsTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func viewDidAppear(_ animated: Bool) {
   
        let left = tabBar.items![0]
        let right = tabBar.items![1]
        let last = tabBar.items![2]

        left.image = #imageLiteral(resourceName: "ic_settings_input_svideo")
        right.image =  #imageLiteral(resourceName: "ic_pan_tool")
        
        left.title = "Tap"
        right.title = "Swipe"
        last.title = "Pinch"
        
        left.badgeColor = .red
        left.badgeValue = "6"
        
    }

}
