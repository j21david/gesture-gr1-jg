//
//  TapGestureViewController.swift
//  gesture-gr1-jg
//
//  Created by Jose Gonzalez on 17/1/18.
//  Copyright © 2018 Jose Gonzalez. All rights reserved.
//

import UIKit

class TapGestureViewController: UIViewController {

    
    
    @IBOutlet weak var touchesLabel: UILabel!
    
    @IBOutlet weak var tapsLabel: UILabel!
    
    @IBOutlet weak var coordLabel: UILabel!
    
    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }



    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        touchesLabel.text = "\(touchCount ?? 0)"
        tapsLabel.text = "\(tapCount ?? 0)"
        
        
    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        
        coordLabel.text = "x: \(x!), y: \(y!)"
        print("x: \(x!), y: \(y!)")
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("terminó!")
    }
    
    
    
    
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        
    customView.backgroundColor = .blue

        
        
    }
    
    
    
    
}
