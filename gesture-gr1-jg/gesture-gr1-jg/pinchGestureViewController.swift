//
//  pinchGestureViewController.swift
//  gesture-gr1-jg
//
//  Created by Jose Gonzalez on 17/1/18.
//  Copyright © 2018 Jose Gonzalez. All rights reserved.
//

import UIKit

class pinchGestureViewController: UIViewController {

    @IBOutlet var initialView: UIView!
    @IBOutlet weak var customView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func pinchAction(_ sender: Any) {
       
        let action = sender as! UIPinchGestureRecognizer
        
     
        
        customView.backgroundColor = .red
       
        if action.state == .began || action.state == .changed {
            
           // action.view?.transform = (action.view?.transform.scaledBy(x: action.scale ,y: action.scale))!
            //action.scale = 1.0
            
            pinch(withScale: action.scale , velocity: action.velocity)
        }
        
    }
            
    
    
    func pinch(withScale scale: CGFloat, velocity: CGFloat){
        print("\(velocity)")
        print("\(scale)")
        
      
        
      
        if(velocity>1.5){
            
            self.customView.transform = CGAffineTransform(scaleX: 100, y: 100)
        }
        if(velocity>1){
            
            self.customView.transform = CGAffineTransform(scaleX: velocity, y: velocity)
        }
        else {
            
            self.customView.transform = CGAffineTransform(scaleX: 1, y: 1)
            
        }
     
        
        
        
        
    }
    
 
    
}
